#!/bin/bash

###
# Gitlab-runner
# Ref: https://docs.gitlab.com/runner/install/linux-repository.html
#
# FreeYeti <yeti@freeyeti.net>
###

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install -y gitlab-runner