# Convenience scripts for Ubuntu

* [Docker](scripts/docker.sh)
* [Kubernetes](scripts/kubernetes.sh)
* [Certbot](scripts/certbot.sh)
* [Docker-compose](scripts/docker-compose.sh)
* [Gitlab-runner](scripts/gitlab-runner.sh)
* [V2ray](scripts/v2ray.sh)